
#include <msp430.h>
#include <legacymsp430.h>

#define R_LED  BIT0
#define G_LED  BIT6

#define N_STATES 4

int state = 0;

void update(void) {

	state = (state + 1) % N_STATES;

	/* first bit set, enable red */
	if (state & 0x1)
		P1OUT |= R_LED;
	else
		P1OUT &= ~R_LED;

	/* second bit set, enable green */
	if (state & 0x2)
		P1OUT |= G_LED;
	else
		P1OUT &= ~G_LED;
}

int main(void) {

	/* Set watchdog timer to interupt at 32ms interval. */
	WDTCTL = WDT_MDLY_32;

	/* set direction to output on both leds */
	P1DIR |= (R_LED | G_LED);

	P1OUT = 0x0;

	/* enable interrupt */
	IE1 |= WDTIE;
	_BIS_SR(LPM0_bits + GIE);

	for(;;);

	return 0;
}

interrupt(WDT_VECTOR) main_isr(void) {

	static int cntr = 0;

	if ((cntr++ % 30) == 0)
		update();
}
