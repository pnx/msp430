
CC 	= msp430-gcc
CFLAGS 	= -Os -Wall -g -mmcu=msp430x2012
LD 	= $(CC)
LDFLAGS =

PROGRAMS = led1.elf led2.elf led3.elf

.PHONY : clean distclean

all : $(PROGRAMS)

led1.elf : led1.o
led2.elf : led2.o
led3.elf : led3.o

%.elf :
	$(LD) -o $@ $^

clean :
	$(RM) *.o

distclean : clean
	$(RM) $(PROGRAMS)
