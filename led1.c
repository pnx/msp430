
#include <msp430.h>

/*
 * P1 bit 0 = red led.
 * P1 bit 6 = green led.
 * P1 bit 3 = button.
 */

int main(void) {
	/* Init watchdog timer to off */
	WDTCTL = WDTPW | WDTHOLD;

	/* set all P1 direction to input. */
	P1DIR = 0x0;

	/* set red led to output */
	P1DIR |= BIT0;

	P1OUT = 0x0;

	for(;;) {
		/*
		 * P1.3 bit set -> button _not_ pressed.
		 */
		if (P1IN & BIT3) {
			P1OUT &= ~BIT0;
		} else {
			P1OUT |= BIT0;
		}
	}

	return 0;
}
