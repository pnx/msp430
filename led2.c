
#include <msp430.h>

/*
 * P1 bit 0 = red led.
 * P1 bit 6 = green led.
 * P1 bit 3 = button.
 */

 #define N_STATES 4

int state = 0;

/*  G | R | state (dec)
 *  0 | 0 | 0
 *  0 | 1 | 1
 *  1 | 0 | 2
 *  1 | 1 | 3
 */

void update(void) {

	static unsigned in_progress = 0;

	if (in_progress)
		return;
	in_progress = 1;

	state = (state + 1) % N_STATES;

	/* first bit set, enable red */
	if (state & BIT0)
		P1OUT |= BIT0;
	else
		P1OUT &= ~BIT0;

	/* second bit set, enable green */
	if (state & BIT1)
		P1OUT |= BIT6;
	else
		P1OUT &= ~BIT6;

	in_progress = 0;
}

void delay(void) {

	int i = 0x6fff;

	while(i--)
		nop();
}

int main(void) {

	/* Init watchdog timer to off */
	WDTCTL = WDTPW | WDTHOLD;

	/* set all P1 direction to input. */
	P1DIR = 0x0;

	/* set direction to output on both leds */
	P1DIR |= (BIT0 | BIT6);

	P1OUT = 0x0;

	for(;;) {
		/*
		 * P1.3 bit set -> button _not_ pressed.
		 */
		if (!(P1IN & BIT3))
			update();

		delay();
	}
	return 0;
}
